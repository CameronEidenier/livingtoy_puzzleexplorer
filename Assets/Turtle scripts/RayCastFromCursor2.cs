﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class RayCastFromCursor2 : MonoBehaviour
{
    public GameObject cardboardMainObject;
    // Use this for initialization
    void Start()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    // Update is called once per frame
    void Update()
    {
        if (Cardboard.SDK.Triggered)
        {
            Ray ray;
            RaycastHit raycastHit;
            print("Mouse Down");
            if (cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled)
            {
                Physics.Raycast(transform.position, transform.forward, out raycastHit);
            }
            else
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Physics.Raycast(ray, out raycastHit);
            }
            if (raycastHit.collider != null)
                {
                    print("Hit something");
                    if (raycastHit.collider.tag == "Character")
                    {
                    raycastHit.collider.transform.GetComponent<Character_Animator>().Victory();
                    }
                    if (raycastHit.collider.tag == "VrToggle")
                    {
                        ARVRToggle();
                    }
            }
        }
    }
    public void ARVRToggle()
    {
        cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
    }
}
